# QEMU/KVM systemd service

.service file that makes it easy to run differently configured QEMU VMs on boot
(or on demand) via systemd. No libvirt required.

The service file is based on [an entry in the ArchWiki](https://wiki.archlinux.org/index.php/QEMU#With_systemd_service),
but in my usecase I needed/wanted firewall rules to be automatically
added/removed.

## "Install"

1. Place `qemu@.service` into `/etc/systemd/system/` 
2. Run `systemd daemon-reload`.
3. Place your vm configuration along the lines of the `example-vm` file in
   `/etc/qemu/vms/`

## Starting

On the fly:

`systemctl start qemu@example-vm.service`

On boot:

`systemctl enable qemu@example-vm.service`
